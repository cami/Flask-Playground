# Flask-Things :-)

**This is just a project to learn some things about Flask! So do not use it for a public service or something like this**

## Installation / Usage

1. Clone this repository and go into the directory
2. `python -m venv env`
3. `source ./venv/bin/activate`
4. Install deps: `pip install -r requirements.txt`

## Development

1. Clone this repository and go into the directory
2. `python -m venv env`
3. `source ./venv/bin/activate`
4. Install deps: `pip install -r requirements.txt.dev`

### Update `requirements.txt` files

The production requirements should not include development only packages. So add further packages if they're only for development

```
pip freeze --exclude autopep8 --exclude pycodestyle --exclude toml > requirements.txt
```

For the development dependencies use the following command:

```
pip freeze > requirements.txt.dev
``` 

## Translation

The following things are some instructions to update the translations

### Extract the strings from the whole application

```
pybabel extract -F babel.cfg -o messages.pot .
```

### Create the needed directories and the `*.po` file (only first time)

```
pybabel init -i messages.pot -d app/translations -l de
```

### Update the translations based on the extraction 

```
pybabel update -i messages.pot -d app/translations/
```

### Use the newly translated things

```
pybabel compile -d app/translations/
```
