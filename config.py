import os


class Config(object):
    DEBUG = True
    LANGUAGES = ['en', 'de']
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'secret'
